package pl.pp.wi.ezi.algorithm;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import pl.pp.wi.ezi.model.Document;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class TFIDFSolTest {

  @Test
  public void should_run_service() {
    TFIDFSol sol = new TFIDFSol(getSomeDocuments());
    sol.go();
  }

  @Test
  public void should_have_3_elements_in_db() {
    List<Document> someDocuments = getSomeDocuments();

    TFIDFSol sol = new TFIDFSol(someDocuments);
    List<Document> db = sol.getDocumentCollection();
    assertThat(db).hasSize(someDocuments.size());

    for (int i = 0; i < db.size(); i++) {
      assertThat(db.get(i).getValue()).isEqualTo(someDocuments.get(i).getValue());
    }
  }

  @Test
  public void should_calculate_tf() {
    TFIDFSol sol = new TFIDFSol(Collections.emptyList());
    ImmutableMap<String, Double> expectedTf = ImmutableMap.<String, Double>builder()
        .put("information", 0.5)
        .put("retrieval", 0.5)
        .put("agency", 1.0)
        .build();

    Map<String, Double> tf = sol.calculateTF(Document.fromString("agency information retrieval agency"));

    for (String key : tf.keySet()) {
      assertThat(tf.get(key)).isEqualTo(expectedTf.get(key));
    }
  }

  @Test
  public void should_calculate_more_complex_tf() {
    String stemmedDocument = "Fruit fly fly around in swarms. When fly they flap their wings 220 times a second";

    ImmutableMap<String, Double> expectedTf = ImmutableMap.<String, Double>builder()
        .put("bee", 0.)
        .put("wasp", 0.)
        .put("fly", 1.0)
        .put("fruit", 0.3333333333333333)
        .put("like", 0.)
        .build();

    TFIDFSol sol = new TFIDFSol(Collections.emptyList());
    Map<String, Double> tf = sol.calculateTF(Document.fromString(stemmedDocument));
    for (String key : expectedTf.keySet()) {
      assertThat(tf.getOrDefault(key, 0.)).isEqualTo(expectedTf.get(key));
    }
  }

  @Test
  public void should_calculate_idf() {
    ImmutableMap<String, Double> expectedTf =
        ImmutableMap.<String, Double>builder()
            .put("information", Math.log10(2))
            .put("retrieval", 0.)
            .put("agency", Math.log10(2))
            .build();

    TFIDFSol sol = new TFIDFSol(getSomeDocumentsFromEziClass());
    Map<String, Double> idfMap = sol.getIdfMap();
    for (String key : idfMap.keySet()) {
      assertThat(idfMap.get(key)).isEqualTo(expectedTf.get(key));
    }
  }

  private List<Document> getSomeDocumentsFromEziClass() {
    return Stream.of(
        "information retrieval information retrieval",
        "retrieval retrieval retrieval retrieval",
        "agency information retrieval agency",
        "retrieval agency retrieval agency")
        .map(Document::fromString)
        .collect(Collectors.toList());
  }

  private List<Document> getSomeDocuments() {
    return Stream.of(
        "Equations",
        "Equations",
        "Algorithms Theory Implementation Application",
        "Partial Differential Equations",
        "Algorithms Introduction",
        "Introduction Systems Problems",
        "Problems Algorithms Implementation",
        "Methods Systems Ordinary Differential Equations",
        "Nonlinear Systems",
        "Ordinary Differential Equations",
        "Oscillation Theory Delay Differential Equations",
        "Oscillation Theory Delay Differential Equations",
        "Nonlinear Partial Differential Equations",
        "Methods Differential Equations",
        "Differential Equations",
        "Integral Problems",
        "Integral Application Theory")
        .map(Document::fromString)
        .collect(Collectors.toList());
  }


}