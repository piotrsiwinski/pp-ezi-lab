package pl.pp.wi.ezi.service;

import org.junit.Before;
import org.junit.Test;
import pl.pp.wi.ezi.model.Document;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class DocumentServiceTest {

  private DocumentService fileService;

  @Before
  public void before() {
    fileService = new DocumentService();
  }

  @Test
  public void should_map_to_documents_from_list_of_string() {
    List<Document> documents = fileService.mapToDocumentList(getTestList());
    assertThat(documents).hasSize(3);
//    assertThat(documents.get(0).getValue()).isEqualTo(
//        "David W. Aha:  Machine Learning Page"
//            + "Machine Learning Resources. Suggestions welcome. ... (WizRule); ZDM Scientific"
//            + "Ltd. Conference Announcements. Courses on Machine Learning. Data Repositories. ..."
//            + "Description: Comprehensive machine learning resources from Applications to Tutorials.");
//    assertThat(documents.get(1).getValue()).isEqualTo(
//        "Home Pages of ML & CBR Folks"
//            + "Machine Learning and Case-Based Reasoning Home Pages. All 834 entries | Dedicated"
//            + "interface | Geographic location (Please send edits to David W. Aha). ..."
//            + "Description: A list of home pages for people in machine learning and case-based reasoning.");
//    assertThat(documents.get(2).getValue()).isEqualTo(
//        "UCI Machine Learning Repository"
//            + "Welcome to the UCI Machine Learning Repository! ... The majority of the entries in the"
//            + "repository were contributed by machine learning researchers outside of UCI. ..."
//            + "Description: A repository of databases, domain theories and data generators that are used by the machine learning...");

  }

  List<String> getTestList() {
    return Arrays.asList(
        "David W. Aha:  Machine Learning Page",
        "Machine Learning Resources. Suggestions welcome. ... (WizRule); ZDM Scientific",
        "Ltd. Conference Announcements. Courses on Machine Learning. Data Repositories. ...",
        "Description: Comprehensive machine learning resources from Applications to Tutorials.",
        "\n",
        "Home Pages of ML & CBR Folks",
        "Machine Learning and Case-Based Reasoning Home Pages. All 834 entries | Dedicated",
        "interface | Geographic location (Please send edits to David W. Aha). ...",
        "Description: A list of home pages for people in machine learning and case-based reasoning.",
        "\n",
        "UCI Machine Learning Repository",
        "Welcome to the UCI Machine Learning Repository! ... The majority of the entries in the",
        "repository were contributed by machine learning researchers outside of UCI. ...",
        "Description: A repository of databases, domain theories and data generators that are used by the machine learning..."
    );
  }

}