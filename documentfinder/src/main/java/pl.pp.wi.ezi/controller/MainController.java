package pl.pp.wi.ezi.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import opennlp.tools.stemmer.PorterStemmer;
import pl.pp.wi.ezi.algorithm.DocScore;
import pl.pp.wi.ezi.algorithm.TFIDFSol;
import pl.pp.wi.ezi.model.Document;
import pl.pp.wi.ezi.model.Term;
import pl.pp.wi.ezi.service.DocumentService;
import pl.pp.wi.ezi.util.JavaFxUtils;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Vector;

import static pl.pp.wi.ezi.util.JavaFxUtils.setTextAreaText;

public class MainController {

  @FXML
  public Label resultLabel;
  @FXML
  public Button searchButton;
  @FXML
  private TextArea documentTextArea;
  @FXML
  private TextArea stemmedDocumentTextArea;
  @FXML
  public TextArea termTextArea;
  @FXML
  public TextArea dbTextArea;
  @FXML
  private TextField queryTextField;
  @FXML
  private TextArea queryTextArea;
  @FXML
  private ScrollPane mainGridPane;


  private DocumentService fileService = new DocumentService();

  private List<Document> documents;
  private List<Document> stemmedDocuments;


  public void openDocumentSet(MouseEvent mouseEvent) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Open Resource File");

    try {
      // get file
      File file = Optional.ofNullable(
          fileChooser
              .showOpenDialog(mainGridPane.getScene().getWindow()))
          .orElseThrow(RuntimeException::new);
      // parse file to list of documents
      List<Document> rawDocuments = fileService.getDocuments(file);
      setTextAreaText(documentTextArea, rawDocuments);
      // get stemmed documents
      List<Document> stemDocuments = fileService.stemDocuments(rawDocuments);
      setTextAreaText(stemmedDocumentTextArea, stemDocuments);

      // keep documents in memory for further operations
      this.stemmedDocuments = stemDocuments;
      this.documents = rawDocuments;
    } catch (RuntimeException e) {
      JavaFxUtils.showErrorAlert();
    }
  }


  public void openTermSet(MouseEvent mouseEvent) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Open Resource File");
    try {
      File file = Optional.ofNullable(
          fileChooser
              .showOpenDialog(mainGridPane.getScene().getWindow()))
          .orElseThrow(RuntimeException::new);
      List<Term> terms = fileService.getTerms(file);

      PorterStemmer stemmer = new PorterStemmer();
      List<Term> stemmedWords = new LinkedList<>();
      for (Term t : terms) {
        String stem = stemmer.stem(t.getValue());
        stemmedWords.add(Term.builder().value(stem).build());
      }

      StringBuilder sb = new StringBuilder();
      stemmedWords.forEach(t -> sb.append(t).append("\n"));
      termTextArea.setText(sb.toString());
    } catch (RuntimeException ex) {
      JavaFxUtils.showErrorAlert();
    }

  }

  public void onSearchClicked(MouseEvent mouseEvent) {
    TFIDFSol tfidfSol = new TFIDFSol(this.stemmedDocuments);
    String text = this.queryTextField.getText();

    Document stemmedDocument = fileService.stemDocument(Document.fromString(text));
    Vector<DocScore> rank = tfidfSol.rank(stemmedDocument);

    StringBuilder stringBuilder = new StringBuilder();
    for (DocScore docScore : rank) {
      stringBuilder
          .append(String.format(
              "%d, %.5f, %s\n",
              docScore.getDocId(),
              docScore.getScore(),
              this.documents.get(docScore.getDocId())));
    }
    this.queryTextArea.setText(stringBuilder.toString());
    this.resultLabel.setText(String.format("Found: %d results", rank.size()));

  }
}
