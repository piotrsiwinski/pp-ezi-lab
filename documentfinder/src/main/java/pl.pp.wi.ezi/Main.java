package pl.pp.wi.ezi;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import pl.pp.wi.ezi.util.ResourceUtil;

public class Main extends Application {

  private static final String MAIN_WINDOW_PATH = "view/MainWindow.fxml";

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    Parent root = FXMLLoader.load(ResourceUtil.getResource(MAIN_WINDOW_PATH));
    Screen screen = Screen.getPrimary();
    Rectangle2D bounds = screen.getVisualBounds();
    primaryStage.setTitle("Document finder");
    primaryStage.setScene(new Scene(root, 800, 600));
    primaryStage.setX(bounds.getMinX());
    primaryStage.setY(bounds.getMinY());
    primaryStage.setWidth(bounds.getWidth());
    primaryStage.setHeight(bounds.getHeight());
    primaryStage.show();
  }
}
