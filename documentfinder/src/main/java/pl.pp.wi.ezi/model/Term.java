package pl.pp.wi.ezi.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Term {
  private String value;
}
