package pl.pp.wi.ezi.model;

import lombok.Value;

import static pl.pp.wi.ezi.util.StringUtils.toLowerCaseWithoutWhiteSpace;

@Value
public class Document {
  private String value;

  public static Document fromString(String s) {
    return new Document(s.trim());
  }

  @Override
  public String toString() {
    return value;
  }
}
