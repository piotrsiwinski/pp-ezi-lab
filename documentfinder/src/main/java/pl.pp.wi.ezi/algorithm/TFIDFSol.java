package pl.pp.wi.ezi.algorithm;

import pl.pp.wi.ezi.model.Document;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import static java.util.Objects.requireNonNull;
import static pl.pp.wi.ezi.util.ListUtil.getByIndexOrZero;

public class TFIDFSol {

  private List<Document> documentCollection;// the document collection
  private TreeMap<String, Double> idfMap = new TreeMap<>(); // idf value for each term in the vocabulary
  private TreeMap<String, Set<Integer>> invertedFileMap = new TreeMap<>(); // term -> id Dokumentów zawierające termin - do idf - docIds of docs containing the term
  private Vector<Map<String, Double>> tfMap = new Vector<>(); // term x docId matrix with term frequencies

    public TFIDFSol(List<Document> documents) {
      documentCollection = requireNonNull(documents);
      init();
    }

  @Deprecated
  TFIDFSol(String pathToFile) {
    initDb(pathToFile);
        init();
    }

  // init tfMap, invertedFileMap, and idfMap
  private void init() {
    int docId = 0;
    // for all docs in the database
    for (Document doc : documentCollection) {
      // get the tfs for a doc
      Map<String, Double> termFreqs = calculateTF(doc);

      // add to global tfMap vector
      tfMap.add(termFreqs);

      // for all terms
      for (String term : termFreqs.keySet()) {
        // add the current docID to the posting list
        Set<Integer> docIds = invertedFileMap.getOrDefault(term, new TreeSet<>());
        docIds.add(docId);
        invertedFileMap.put(term, docIds);
      }
      docId++;
    }

    // calculate idfMap
    for (Map.Entry<String, Set<Integer>> entry : invertedFileMap.entrySet()) {
      String term = entry.getKey();
      // get the size of the posting list, i.e. the document frequency
      int termFrequency = entry.getValue().size();
      // TODO write the formula for calculation of IDF - DONE AND TESTED
      idfMap.put(term, calculateIdf(termFrequency));
    }
  }

  //  Miara IDF obliczana jest jako logarytm ze stosunku liczby wszystkich dokumentów kolekcji ) do liczby dokumentów zawierających term
  double calculateIdf(int termFrequency) {
    return Math.log10(this.documentCollection.size() / (double) termFrequency);
  }

  double calculateTfIdf(double tf, double idf) {
    return tf * idf;
  }

  public Map<String, Double> getIdfMap() {
    return new TreeMap<>(idfMap);
    }

    // calculates the similarity between two vectors
    // each vector is a term -> weight map
    // each vector is a term -> TFIDF map
    private double similarity(Map<String, Double> v1, Map<String, Double> v2) {
        double sum = 0;
        // iterate through one vector
        for (Map.Entry<String, Double> entry : v1.entrySet()) {
            String term = entry.getKey();
            Double w1 = entry.getValue();
            // multiply weights if contained in second vector
            Double w2 = v2.get(term);
            if (w2 != null)            	
                sum += w1 * w2;
        }
      // TODO - similarity method - write the formula for computation of cosinus - DONE AND NOT TESTED
        // note that v.values() is Collection<Double> that you may need to calculate length of the vector
        // take advantage of vecLength() function

        List<Double> v1Values = new ArrayList<>(v1.values());
        List<Double> v2Values = new ArrayList<>(v2.values());
        double maxLength = Math.max(v1Values.size(), v2Values.size());

        double sumVec = 0;
        for (int i = 0; i < maxLength; i++) {
          sumVec += getByIndexOrZero(v1Values, i) * getByIndexOrZero(v2Values, i);
        }
      return sumVec / (vecLength(v1Values) * vecLength(v2Values));
    }

    // returns the length of a vector
    private double vecLength(Collection<Double> vec) {
        double sum = 0;
        for (Double d : vec) {
            sum += Math.pow(d, 2);
        }
        return Math.sqrt(sum);
    }

    // returns the idf of a term
    private double idf(String term) {
      return idfMap.get(term);
    }

    // calculates the document vector for a given docID
    private Map<String, Double> getDocVec(int docId) {
      Map<String, Double> vec = new TreeMap<>();

        // get all term frequencies
      Map<String, Double> termFreqs = tfMap.elementAt(docId);

      // for each term, tfMap * idf
        for (Map.Entry<String, Double> entry : termFreqs.entrySet()) {
            String term = entry.getKey();
          //TODO - DONE compute tfidf value for a given term - DONE AND TESTED
          double tf = entry.getValue();
          double tfidf = calculateTfIdf(tf, idf(term));
          vec.put(term, tfidf);
        }
        return vec;
    }

    // returns the term frequency for a term and a docID
    private double calculateTF(String term, int docId) {
      return tfMap.elementAt(docId).getOrDefault(term, 0.);
    }

    // calculates the term frequencies for a document
    Map<String, Double> calculateTF(Document doc) {
      Map<String, Integer> termFreqs = new HashMap<>();
      int max = 0;

        // tokenize document
      StringTokenizer st = new StringTokenizer(doc.getValue(), " ");

        // for all tokens
        while (st.hasMoreTokens()) {
            String term = st.nextToken();
            // count the max term frequency
          int count = termFreqs.getOrDefault(term, 0);
            count++;
            termFreqs.put(term, count);
          if (count > max) {
            max = count;
          }
        }

      // normalize tfMap
      //TODO write the formula for normalization of TF - DONE AND TESTED
      Map<String, Double> tfResult = new TreeMap<>();
      for (String term : termFreqs.keySet()) {
        tfResult.put(term, (double) termFreqs.get(term) / max);
      }
      return tfResult;
    }


  public List<Document> getDocumentCollection() {
    return new ArrayList<>(this.documentCollection);
  }

  // ranks a query to the documents of the database
  public Vector<DocScore> rank(Document queryDocument) {
    // get term frequencies for the query terms
    Map<String, Double> queryTermsFrequencyMap = calculateTF(queryDocument);

    // construct the query vector
    // the query vector
    Map<String, Double> queryVec = new TreeMap<>();

    // iterate through all query terms
    for (Map.Entry<String, Double> entry : queryTermsFrequencyMap.entrySet()) {
      String term = entry.getKey();
      //TODO compute tfidf value for terms of query - DONE AND TESTED
      double tf = entry.getValue();
      double idf = calculateIdf(entry.getValue().intValue());
      double tfidf = calculateTfIdf(tf, idf);
      queryVec.put(term, tfidf);
    }

    TreeSet<String> queryTerms = new TreeSet<>(queryTermsFrequencyMap.keySet());

    // from the inverted file get the union of all docIDs that contain any query term
    Set<Integer> union = Optional
        .ofNullable(invertedFileMap.get(queryTerms.first()))
        .orElseGet(TreeSet::new);

    for (String term : queryTerms) {
      union.addAll(Optional.ofNullable(invertedFileMap.get(term)).orElse(Collections.emptySet()));
    }

    // calculate the scores of documents in the union
    Vector<DocScore> scores = new Vector<>();
    for (Integer i : union) {
      scores.add(new DocScore(similarity(queryVec, getDocVec(i)), i));
    }
    // sort and print the scores
    Collections.sort(scores);
    for (DocScore docScore : scores) {
      System.out.printf("score of doc %d = %s%n", docScore.docId, docScore.score);
    }
    return scores;
  }

  public Document getDocumentById(int id) {
    return documentCollection.get(id);
  }

  @Deprecated
  void go() {
    // print the database
    printDB();

    // idfMap and tfs
    System.out.println("IDFs:");
    // print the vocabulary
    printVoc();

    printTf();

    // similarities for different queries
    rank(Document.fromString("Differential Equations"));
  }

  @Deprecated
  private void printTf() {
    System.out.println("\nTFs for Equations:");
    for (int i = 0; i < documentCollection.size(); i++) {
      System.out.printf("Equations: doc %d : %s%n", i, calculateTF("equations", i));
    }
  }

  // inits database from textfile
  @Deprecated
  private void initDb(String filename) {
    documentCollection.clear();
    try (BufferedReader br = Files.newBufferedReader(Paths.get(filename))) {
      while (br.ready()) {
        String doc = br.readLine().trim();
        documentCollection.add(Document.fromString(doc));
      }
    } catch (FileNotFoundException e) {
      System.out.println("No database available.");
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  // lists the vocabulary
  @Deprecated
  private void printVoc() {
    System.out.println("Vocabulary:");
    for (Map.Entry<String, Double> entry : idfMap.entrySet()) {
      System.out.printf("%s, idf = %s%n", entry.getKey(), entry.getValue());
    }
  }

  // lists the database
  @Deprecated
  private void printDB() {
    System.out.printf("size of the database: %d%n", documentCollection.size());
    for (int i = 0; i < documentCollection.size(); i++) {
      System.out.printf("doc %d: %s%n", i, documentCollection.get(i).getValue());
    }
    System.out.println();
  }
}

