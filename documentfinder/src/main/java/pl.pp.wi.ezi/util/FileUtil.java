package pl.pp.wi.ezi.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public class FileUtil {
  public static List<String> readAllLines(File f) {
    try {
      return Files.readAllLines(Paths.get(f.toURI()));
    } catch (IOException e) {
      return Collections.emptyList();
    }
  }
}
