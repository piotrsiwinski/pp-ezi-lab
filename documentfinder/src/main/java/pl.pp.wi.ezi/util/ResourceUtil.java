package pl.pp.wi.ezi.util;

import java.net.URL;

public class ResourceUtil {
  public static URL getResource(String path) {
    return ResourceUtil.class.getClassLoader().getResource(path);
  }
}
