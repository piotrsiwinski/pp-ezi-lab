package pl.pp.wi.ezi.util;

import java.util.List;

public class ListUtil {
  private ListUtil() {
    throw new AssertionError();
  }

  public static double getByIndexOrZero(List<Double> v1Values, int i) {
    return i < v1Values.size() ? v1Values.get(i) : 0;
  }
}
