#EZI Labs
Programming part from EZI 
Authors: Piotr Siwiński, Mateusz Byczkowski
## Runing program
Clone repository:
``` 
git clone https://piotrsiwinski@bitbucket.org/piotrsiwinski/pp-ezi-lab.git
```

To run openNLP:
``` 
 cd pp-ezi-lab/opennlp
./mvnw exec:java
```

To run documentFinder (from root of project):
``` 
 cd documentfinder/
./mvnw exec:java
```

#Troubleshooting
In case of problems try:
```
./mvnw  clean package

```